import Pkg;

#Pkg.add("Images")
#Pkg.add("Metalhead")
#Pkg.add("CSV")


using CuArrays
using Flux, Flux.Tracker
using Flux: onehotbatch, chunk, onecold, crossentropy, throttle
using Flux:@epochs
using Statistics
using Images
using Metalhead
using Base
using Base.Iterators: repeated, Stateful, partition
using Statistics
using BSON: @save, @load
using ImageView
using Images
using ProgressMeter
using Random
import CSV


function loadDataset(trainCSVFilePath, imagesFolderRoot, restrictedImagesLabels)
    labelsVsImagesNames = CSV.File(trainCSVFilePath)
    #output = Array{any, 2}(0, undef)
    outputLabels = []
    outputImages = []
    for row in labelsVsImagesNames
        imageName = row.Image
        id = row.Id
        if id in restrictedImagesLabels
            imagePath = imagesFolderRoot*imageName
            image = load(imagePath)
            image = Metalhead.preprocess(image)
            labelHotEncoded = onehotbatch([id], restrictedImagesLabels)
            push!(outputLabels,labelHotEncoded)
            push!(outputImages, image)
        end
    end
    #println(labelsVsImagesNames)
    outputImages, outputLabels
end

function viewImage(imageData)
    imshow(colorview(RGB,PermutedDimsArray(imageData[:,:,:,1],[3,2,1])./255))
end

function divideData(trainImages, trainLabels, trainRatio)
    categoriesImagesCount = zeros(12)
    for hotEncoding in trainLabels
        #println(hotEncoding)
        label_index = onecold(hotEncoding, 1:12)[1]
        #println(label_index)
        categoriesImagesCount[label_index] += 1
    end
    trainImagesCountForEachCategory = floor.(Int,categoriesImagesCount.*trainRatio)
    enconteredCategoriesImagesCount = zeros(12)
    outputTrainImages = []
    outputTrainLabels = []
    outputTestImages = []
    outputTestLabels  = []
    for (image, labelEncoded) in zip(trainImages, trainLabels)
        label_index = onecold(labelEncoded, 1:12)[1]
        current_count = enconteredCategoriesImagesCount[label_index]
        if current_count <= trainImagesCountForEachCategory[label_index]
            push!(outputTrainImages, image)
            push!(outputTrainLabels, labelEncoded)
        else
            push!(outputTestImages, image)
            push!(outputTestLabels, labelEncoded)
        end
        enconteredCategoriesImagesCount[label_index] += 1
    end
    outputTrainImages, outputTrainLabels, outputTestImages, outputTestLabels
end

function createModel()
    #model_part1 = VGG19().layers[1:end-2]
    #model_part2 = Dense(4096, 1)
    model_part1 = Chain(
        Conv((3,3), 3=>16, relu),
        x -> maxpool(x, (2,2)),
        Conv((3,3), 16=>32, relu),
        x -> maxpool(x, (2,2)),
        Conv((3,3), 32=>48, relu),
        x -> maxpool(x, (2,2)),
        Conv((3,3), 48=>10, relu),
        x -> reshape(x, :, size(x, 4)),
        Dense(5760, 1000, relu),
        Dense(1000, 200),
    )
    model_part2 = Dense(200, 1)
    model = Array{Any, 1}(undef, 0)
    #model = [model_part1, model_part2]
    push!(model, model_part1)
    push!(model, model_part2)
    return model
end

function scoreImages(model, img1, img2)
    #println("In score Images")
    x1 = model[1](img1 |> gpu)
    x2 = model[1](img2 |> gpu)
    #println("Before fc")
    fc = abs.(model[2](abs.(x1.-x2)))
    #println("After fc")
    #println("fc: $fc")
    return fc
end

function loss(model, img1, img2, isSimilar, margin)
    #println("loss Entrance")
    distance = scoreImages(model, img1, img2)
    #println("loss After computing scores")
    #isSimilar = Float32(isSimilar)
    #println("isSimilar: $isSimilar")
    #println("distance:")
    isSimilar = isSimilar |> gpu
    #println("loss after isSimilar |> gpu")
    cost = 0.5 .* (isSimilar .* distance .^ 2 +  (1 .- isSimilar).* max.(0, margin .- distance) .^ 2)
    #println("inside loss")
    #@show isSimilar
    #@show distance
    #@show cost
    #println("end loss")
    #println("loss after cost computation")
    #println("After Cost")
    return cost
end
#globalTrainImageCounter = 0
function batchLoss(model, batch, margin)
    #println("batch: $batch")
    #println("Batch Entrance")
    firstGroup = batch[1]
    secondGroup = batch[2]
    isSimilarGroup = batch[3]
    BatchLength = length(isSimilarGroup)
    lossesSum = 0.0
    # for (firstImg, secondImg, isSimilar) in zip(firstGroup, secondGroup, isSimilarGroup)
    #     #println("Before lossSum")
    #     #globalTrainImageCounter += 1
    #     imageLoss = loss(model, firstImg, secondImg, isSimilar, margin)[1]
    #     lossesSum += imageLoss
    #     #println("Image: $globalTrainImageCounter loss is: $imageLoss")
    #     #println("After lossSum")
    # end
    losses = loss(model, firstGroup, secondGroup, isSimilarGroup, margin)
    #println("Batch After loss")
    #finalLoss = lossesSum / BatchLength
    finalLoss = mean(losses)
    #println("Batch Exit")
    #println("Batch loss is: $finalLoss")
    @show finalLoss
    return finalLoss
end

function createTwoTrainImgDataset(trainImages, trainLabels)
    datasetLength = length(trainImages)
    dataset = []
    i = 1
    while i <= (datasetLength - 1)
        #println(trainLabels[i])
        firstImgLabel = onecold(trainLabels[i], 1:12)[1]
        firstImg = trainImages[i]
        j = i + 1
        while j <= datasetLength
            secondImgLabel = onecold(trainLabels[j], 1:12)[1]
            secondImg = trainImages[j]
            isSimilar = (firstImgLabel == secondImgLabel)
            #println("Current Image: $(i*j)")
            #push!(dataset, firstImg)
            #push!(dataset, secondImg)
            #push!(dataset, isSimilar)
            push!(dataset, [firstImg, secondImg, isSimilar])
            j += 1
        end
        i += 1
    end
    #reshape(dataset, :, 3)
    dataset
end

function createTwoTestImgDataset(trainImages, trainLabels, testImages, testLabels)
    datasetLengthTrain = length(trainImages)
    datasetLengthTest = length(testImages)
    dataset = []
    i = 1
    #println("start createTwoTestImgDataset")
    while i <= datasetLengthTrain
        #println(trainLabels[i])
        firstImgLabel = onecold(trainLabels[i], 1:12)[1]
        firstImg = trainImages[i]
        j = 1
        while j <= datasetLengthTest
            secondImgLabel = onecold(testLabels[j], 1:12)[1]
            secondImg = testImages[j]
            isSimilar = (firstImgLabel == secondImgLabel)
            #@show trainLabels[i]
            #@show testLabels[j]
            #@show firstImgLabel
            #@show secondImgLabel
            #@show isSimilar
            #println("Current Image: $(i*j)")
            #push!(dataset, firstImg)
            #push!(dataset, secondImg)
            #push!(dataset, isSimilar)
            push!(dataset, [firstImg, secondImg, isSimilar])
            j += 1
        end
        i += 1
    end
    #reshape(dataset, :, 3)
    #println("end createTwoTestImgDataset")
    dataset
end

function classifyImage(model, img, trainImages, trainLabels, K, progressBar)
    categoriesDistances = ones(12) .* 1e8
    i = 1
    while i <= length(trainImages)
        trainImg = trainImages[i]
        if img === trainImg
            next!(progressBar)
            continue
        end
        label_index = onecold(trainLabels[i], 1:12)[1]
        #println("Before Calculating Score in classifyImage")
        d = scoreImages(model, img, trainImg)
        #println("After Calculating Score in classifyImage")
        categoriesDistances[label_index] = min(Tracker.data(d[1]), categoriesDistances[label_index])
        #println("grabbing the distance and calculating the min distance")
        i += 1
        next!(progressBar)
    end
    scoresAndCategoriesDistances = collect(zip(categoriesDistances, 1:12))
    sortedSCoresAndLabelIndices = collect(zip(sort(scoresAndCategoriesDistances)...))
    #println("After sorting the scores")
    topKScores = sortedSCoresAndLabelIndices[1][1:K]
    topKIndices = sortedSCoresAndLabelIndices[2][1:K]
    #println("After grabbing the top indices")
    return topKIndices
end

function classifyImageTwoWay(model, img1, img2, margin)
    d = Tracker.data(scoreImages(model, img1, img2))
    isSimilar = (d .<= margin)
    #println("classifyImageTwoWay distance is $d")
    #println("isSimilar is $isSimilar")
    return isSimilar
end

function isModelCorrect(model, img, label, trainImages, trainLabels, K, progressBar)
    predictedClasses = classifyImage(model, img, trainImages, trainLabels, K, progressBar)
    label_index = onecold(label, 1:12)[1]
    isCorrect = label_index in predictedClasses
    return isCorrect
end

function isModelCorrectTwoWay(model, img1, img2, isSimilar, margin)
    isSimilar = isSimilar |> gpu
    classificationResult = classifyImageTwoWay(model, img1, img2, margin)
    #@show typeof(classificationResult)
    #@show typeof(isSimilar)
    #@show classificationResult
    #@show isSimilar
    isCorrectMean = mean(classificationResult .== isSimilar)
    #println("isModelCorrectTwoWay start")
    #@show classificationResult
    #@show isSimilar
    #@show isCorrectMean
    #println("isModelCorrectTwoWay end")
    return isCorrectMean
end

function accuracy(model, testImages, testLabels, trainImages, trainLabels, K)
    p = Progress(length(testImages) * length(trainImages), 1, "Processing accuracy on test images ...", 0)
    totalNumberOfImages = length(testImages)
    numberOfCorrectImages = 0
    i = 1
    while i <= totalNumberOfImages
        numberOfCorrectImages += isModelCorrect(model, testImages[i], testLabels[i], trainImages, trainLabels, K, p)
        i += 1
    end
    return numberOfCorrectImages * 100.0 / totalNumberOfImages
end

function accuracyTwoWay(model, twoWayTestDataset, margin)
    #println("begin AccuracyTwoWay")
    p = Progress(length(twoWayTestDataset), 1, "Processing test batchs...", 0)
    totalNumberOfImages = length(twoWayTestDataset)
    numberOfCorrectImages = 0.0
    counter = 1
    for (img1, img2, isSimilar) in twoWayTestDataset
        imageCorrectMeanPerBatch = isModelCorrectTwoWay(model, img1, img2, isSimilar, margin)
        numberOfCorrectImages += Float32(imageCorrectMeanPerBatch)
        #println("Accuracy image count: $(counter)/$(totalNumberOfImages), imageCorrect: $(imageCorrectMeanPerBatch)")
        next!(p)
        counter += 1
    end
     return numberOfCorrectImages / totalNumberOfImages
 end

function trainSiamese(model, testImages, testLabels, trainImages, trainLabels, margin, K, twoImgDatasetBatches, testDataTwoWay, learningRate)
    #evalcb = () -> @show(accuracy(model, testImages, testLabels, trainImages, trainLabels, K))
    #evalcb = () -> @show(accuracyTwoWay(model, testDataTwoWay, margin))
    batchCount = 0
    p = Progress(length(twoImgDatasetBatches), 1, "Processing training batches...", 0)
    #evalcb = () -> println("current batch $(batchCount += 1)/$(length(twoImgDatasetBatches))")
     evalcb = () -> next!(p)
    #println(params(model[1]))
    #println("Model second part parameters: $(params(model[2]))")
    params_model = Params(cat([params(model[1])...], [params(model[2])...],dims=1))
    #println("model parameters $(params_model)")
    opt = ADAM(params_model, learningRate)# params(model[2])
    optimizationFunction = (firstImgbatch, secondImgBatch, isSimilarBatch) -> batchLoss(model, [firstImgbatch, secondImgBatch, isSimilarBatch], margin)
    #optimizationFunction = x -> 1
    #print("the first batch length: $(length(collect(twoImgDatasetBatches)[1][1]))")
    #println("before Flux.Train!")
    Flux.train!(optimizationFunction, twoImgDatasetBatches, opt, cb = evalcb)#, [[1,2,3],[5,6,7],[8,9,10]]
    @show(accuracyTwoWay(model, twoImgDatasetBatches, margin))
    @show(accuracyTwoWay(model, testDataTwoWay, margin))
    @show accuracy(model, testImages, testLabels, trainImages, trainLabels, K)
end

function divideIntoBatches(twoImgDataset, batchSize)
    batches = collect(partition(twoImgDataset, batchSize))
    outputBatches = Array{Tuple{Array{Float32, 4}, Array{Float32, 4},Array{Bool,2}},1}(undef, 0)
    batch_counter = 0
    for batch in batches
        #firstImgBatch = cat(batch[:][1], dims = 5)
        #secondImgBatch = cat(batch[:][2], dims = 5)
        #isSimilarBatch = cat(batch[:][3], dims = 1)
        batch = collect(zip(batch...))
        #println("Before cat")
        batch_p1 = cat(batch[1]..., dims = 4)
        #println("After cat part 1")
        batch_p2 = cat(batch[2]..., dims = 4)
        #println("After cat part 2")
        batch_p3 = cat(batch[3]..., dims= 2)
        #println("After cat part 3")
        #batch_new = [batch_p1, batch_p2, batch_p3]
        #batch_new = Array{Any, 1}(undef, 3)
        #batch_new[1] = batch_p1
        #batch_new[2] = batch_p2
        #batch_new[3] = batch_p3
        #batch[1] = convert(Array{Array{Float64,4},1}, collect(batch[1]))
        #batch[2] = convert(Array{Array{Float64,4},1}, collect(batch[2])
        push!(outputBatches, (batch_p1, batch_p2, batch_p3))
        #println("After push batch: $(batch_counter += 1)")
    end
    println("output batches size: $(length(outputBatches))")
    return outputBatches
end

function obtainBalancedDataset(twoImgDataset)
    #isSimilar = collect(zip(twoImgDataset...))[3]
    #trueIndices = findall(isSimilar)
    #trueCount = length(trueIndices)
    trueCount = 0
    for (firstImg, secondImg, isSimilarElement) in twoImgDataset
        trueCount += isSimilarElement
    end
    falseCount = length(twoImgDataset) - trueCount
    elementsPerClassCount = min(trueCount, falseCount)
    encounteredTrueElementsCount = 0
    encounteredFalseElementsCount = 0
    output = Array{Any, 1}(undef, 0)
    @show elementsPerClassCount
    for (firstImg, secondImg, isSimilarElement) in twoImgDataset
        if isSimilarElement == true && encounteredTrueElementsCount <= elementsPerClassCount
             encounteredTrueElementsCount += 1
             push!(output, (firstImg, secondImg, isSimilarElement))
         elseif isSimilarElement == false && encounteredFalseElementsCount <= elementsPerClassCount
             encounteredFalseElementsCount += 1
             push!(output, (firstImg, secondImg, isSimilarElement))
         end
     end
     return output
end

function main()
    trainLabelsFile = "/media/remon/D/Master/Fall 2018/Deeplearning/Assignments/Final-Project/Dataset/train_original.csv"
    trainImagesRoot = "/media/remon/D/Master/Fall 2018/Deeplearning/Assignments/Final-Project/Dataset/train_original/train/"
    target_whale_ids = ["w_1287fbc",
                        "w_98baff9",
                        "w_7554f44",
                        "w_1eafe46",
                        "w_fd1cb9d",
                        "w_693c9ee",
                        "w_ab4cae2",
                        "w_73d5489",
                        "w_3674103",
                        "w_ab6db0f",
                        "w_c0cfd5b",
                        "w_b729b1f"]
    println("Before Loading the dataset")
    imagesData, labelsData = loadDataset(trainLabelsFile, trainImagesRoot, target_whale_ids)
    println("After Loading the dataset")
    trainRatio = 0.8

    #println("First Label $(labelsData[1])")
    #println("Image")
    #viewImage(imagesData[1])

    trainImages, trainLabels, testImages, testLabels = divideData(imagesData, labelsData, trainRatio)
    println("After dividing the main dataset")
    # println("train images length: $(length(trainImages)), train labels length: $(length(trainLabels))")
    # println("test images length: $(length(testImages)), test labels length: $(length(testLabels))")

    println("Before Creating two Image Dataset")
    twoImgDataset = createTwoTrainImgDataset(trainImages, trainLabels)
    println("After Creating two Image Dataset, length: $(length(twoImgDataset))")

    twoImgDataset = shuffle(obtainBalancedDataset(twoImgDataset))
    println("After obtaining balanced training two image dataset, length: $(length(twoImgDataset))")

    batchSize = 32
    #batches = partition(twoImgDataset, batchSize) # check if it is correct
    batches = divideIntoBatches(twoImgDataset, batchSize)
    #batches = twoImgDataset
    println("After Dividing two Image Dataset")
    #println(batches[1])
    testDataTwoWay = createTwoTestImgDataset(trainImages, trainLabels, testImages, testLabels)
    println("After Creating two Image Dataset test, length: $(length(testDataTwoWay))")

    testDataTwoWay = shuffle(obtainBalancedDataset(testDataTwoWay))
    println("After obtaining balanced test two image dataset, length: $(length(testDataTwoWay))")
    #@show collect(zip(twoImgDataset...))[3]
    #@show collect(zip(testDataTwoWay...))[3]
    batchesTest = divideIntoBatches(testDataTwoWay, batchSize)
    #batches = twoImgDataset
    println("After Dividing test two Image Dataset")

    margin = 10
    K = 5
    nEpochs = 10
    model = createModel()
    println("After Creating model")
    model[1] = model[1] |> gpu
    model[2] = model[2] |> gpu
    println("After Transfer model to GPU")
    #print("Model: $model")
    #println("batches : $batches")
    #batches = convert(Array{Any, 1}, batches)
    batches = batches
    #println("After Transfer batches to GPU")
    #testDataTwoWay = testDataTwoWay |> gpu
    #println("After Transfer testDataTwoWay to GPU")
    #println("Transfer to gpu Finished")
    learningRate = 5e-5
    @epochs nEpochs trainSiamese(model, testImages, testLabels, trainImages, trainLabels, margin, K, batches,batchesTest, learningRate)
    @save "model.bson" model
end

main()
